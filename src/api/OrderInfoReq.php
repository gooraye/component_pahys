<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-28 09:55
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\pahys\api;


use by\component\pahys\helper\Obj2ArrayExtendHelper;
use by\infrastructure\interfaces\ObjectToArrayInterface;

class OrderInfoReq implements ObjectToArrayInterface
{
    private $actualPrice;
    private $appKey;
    private $credits;
    private $description;
    private $facePrice;
    private $itemCode;
    private $orderNum;
    private $params;
    private $timestamp;
    private $type;
    private $uid;
    private $waitAudit;

    public function toArray()
    {
        return Obj2ArrayExtendHelper::getArrayFrom($this);
    }

    /**
     * @return mixed
     */
    public function getActualPrice()
    {
        return $this->actualPrice;
    }

    /**
     * @param mixed $actualPrice
     */
    public function setActualPrice($actualPrice)
    {
        $this->actualPrice = $actualPrice;
    }

    /**
     * @return mixed
     */
    public function getAppKey()
    {
        return $this->appKey;
    }

    /**
     * @param mixed $appKey
     */
    public function setAppKey($appKey)
    {
        $this->appKey = $appKey;
    }

    /**
     * @return mixed
     */
    public function getCredits()
    {
        return $this->credits;
    }

    /**
     * @param mixed $credits
     */
    public function setCredits($credits)
    {
        $this->credits = $credits;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getFacePrice()
    {
        return $this->facePrice;
    }

    /**
     * @param mixed $facePrice
     */
    public function setFacePrice($facePrice)
    {
        $this->facePrice = $facePrice;
    }

    /**
     * @return mixed
     */
    public function getItemCode()
    {
        return $this->itemCode;
    }

    /**
     * @param mixed $itemCode
     */
    public function setItemCode($itemCode)
    {
        $this->itemCode = $itemCode;
    }

    /**
     * @return mixed
     */
    public function getOrderNum()
    {
        return $this->orderNum;
    }

    /**
     * @param mixed $orderNum
     */
    public function setOrderNum($orderNum)
    {
        $this->orderNum = $orderNum;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param mixed $params
     */
    public function setParams($params)
    {
        $this->params = $params;
    }

    /**
     * @return mixed
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param mixed $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getWaitAudit()
    {
        return $this->waitAudit;
    }

    /**
     * @param mixed $waitAudit
     */
    public function setWaitAudit($waitAudit)
    {
        $this->waitAudit = $waitAudit;
    }
}