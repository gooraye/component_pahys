<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-27 14:51
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\pahys\api;


use by\component\pahys\helper\SignHelper;
use by\infrastructure\helper\CallResultHelper;

class ScoreApi extends BaseApi
{
    function getVersion()
    {
        return "0.1.0";
    }

    /**
     * 扣积分
     * @param $apiId
     * @param OrderInfoReq $orderInfoReq
     * @return \by\infrastructure\base\CallResult
     * @throws \ErrorException
     */
    public function dec($apiId, OrderInfoReq $orderInfoReq)
    {
        $orderInfo = json_encode($orderInfoReq->toArray());
        $params = [
            '__o_s' => $apiId,
            '__o_v' => '0.1.0',
            'arg1' => $orderInfo
        ];
        $q = $this->encrypt($params, $this->context->getKey());
        $salt = microtime(true);
        $url = $this->getApiUrl() . '?p=' . $this->context->getPartnerId() . '&v=0.1.0' . '&s=' . $salt;
        $hmac = SignHelper::signHmac($q . $salt, $this->context->getKey());
        $url .= '&h=' . $hmac;
        $result = $this->wrapPostAndVerify($url, ['q' => $q]);
        if ($result->isSuccess()) {
            $data = $result->getData();
            $rh = json_decode(urldecode($data['__o_o']), JSON_OBJECT_AS_ARRAY);
            if ($rh['status'] == 'fail') {
                return CallResultHelper::fail($rh['errorMessage'], $rh);
            }
            return CallResultHelper::success($rh);
        }
        return $result;
    }

    public function getApiUrl()
    {
        return $this->context->getBaseHttpUrl() . $this->getApiGroup() . '/' . $this->getApiName();
    }

    function getApiGroup()
    {
        return "merchantlink";
    }

    function getApiName()
    {
        return "deduction";
    }

    /**
     * @param $apiId
     * @param NotifyReq $notifyReq
     * @return \by\infrastructure\base\CallResult
     * @throws \ErrorException
     */
    public function queryResult($apiId, NotifyReq $notifyReq)
    {
        $salt = microtime(true);
        $url = $this->context->getBaseHttpUrl() . $this->getApiGroup() . '/callBack' . '?p=' . $this->context->getPartnerId() . '&v=0.1.0' . '&s=' . $salt;

        $orderInfo = json_encode($notifyReq->toArray());
        $params = [
            '__o_s' => $apiId,
            '__o_v' => '0.1.0',
            'arg1' => $orderInfo
        ];
        $q = $this->encrypt($params, $this->context->getKey());
        $hmac = SignHelper::signHmac($q . $salt, $this->context->getKey());
        $url .= '&h=' . $hmac;

        $result = $this->wrapPostAndVerify($url, ['q' => $q]);
        if ($result->isSuccess()) {
            $data = $result->getData();
            if (is_array($data) && trim(urldecode($data['__o_o']), "\"") == 'ok') {
                return CallResultHelper::success($data);
            }
            return CallResultHelper::fail("[PAHYS]兑换失败", $data);
        }
        return $result;

    }

}